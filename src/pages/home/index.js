import React from 'react'
import { Skills } from '../../components/Skills';
import { Portafolio } from '../../components/Portafolio';
import imagen from  '../../assets/img/felipeLarraguibel.jpeg'
import './home.css';


export const Home = () => {
    return (
        <>
       
       
        <div className="homeContainer">
            <div className="row">
            <p className="letraPrincipal">
            
            Felipe Larraguibel<br/>
            Desarrollador FullStack.
            </p>
            </div>
        </div>

    <div className="contenedorAbout">
        <div className="col-md-12">
        <p className="señales">De Coquimbo Soy</p>
        <div className="text-center">
        <img className="imagen rounded" src={ imagen } alt=""/>
       </div>
        </div>
        <h3 className="prueba">Desarrollador Web</h3>
        
    </div>

    <Skills/>

    <Portafolio/>
        
    </>
    )
}
